# README for the Frontend of tuency

### Install Debian dependencies
* [Yarn-Classic via Debian Package](https://classic.yarnpkg.com/en/docs/install/#debian-stable) will install nodejs as dependency.

## Project setup
```
cd client
yarn install
cp dot.env.example .env
```
### Compiles and hot-reloads for development
```
yarn serve
```
Dev-server uses the `VUE_BACKEND_API_URL` variable in `.env` file to communicate with the backend endpoints.
Adjust this due to the backend dev-server if necessary.
### Compiles and minifies for production
```
yarn build
```
### Lints and fixes files
```
yarn lint
```

## Show different logos and colours on the different pages.

For this you need to configure a path without authentication for each page in the server_block of openresty.
Look under [server_block, Resource path without authentication](../docs/examples/server_block).

The client includes the files resources/client_style.css and resources/logo.png under the configured path. Examples of these files can be found at [../docs/examples/tuency_resources](../docs/examples/tuency_resources).

To test this look under [../docs/TestMultipleHosts.md](../docs/TestMultipleHosts.md)

## Third party code, licensing info

The initial base of the frontend was created 2020-09 with `vue/cli create`.
[vue-cli](https://github.com/vuejs/vue-cli) is licenced under
[MIT](../LICENSES/MIT.xt) and

    Copyright (c) 2017-2020, Yuxi (Evan) You
