import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueClipboard from "vue-clipboard2";
import { suggestLightTheme } from "./lib/theme-helper";

Vue.config.productionTip = false;
Vue.use(VueClipboard);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");

/*
 * Updating color scheme in theme store when external css
 * theme background is too bright.
 */
document.onreadystatechange = () => {
  if (document.readyState == "complete" && suggestLightTheme()) {
    store.commit("theme/enableDarkTheme", false);
  }
};
