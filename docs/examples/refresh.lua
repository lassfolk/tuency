--[[
    Remove the current session to delete the tokens
    Uses the following nginx variables:
        -client_id: KeyCloak client id
        -redirect_uri: Redirect uri after logout
        -keycloak_url: URL to the keycloak instance
        -keycloak_realm: The realm to use for authentication
]]
local opts = {
    redirect_uri = "/redirect",
    accept_none_alg = true,
    discovery = ngx.var.keycloak_url .. "/auth/realms/" .. ngx.var.keycloak_realm .. "/.well-known/openid-configuration",
    client_id = ngx.var.client_id,
    logout_path = "/logout",
    access_token_expires_leeway = 60,
    renew_access_token_on_expiry = true,
    redirect_after_logout_uri = ngx.var.keycloak_url .. "/auth/realms/" .. ngx.var.keycloak_realm .. "/protocol/openid-connect/logout?redirect_uri=" .. ngx.var.redirect_uri,
    redirect_after_logout_with_id_token_hint = false,
    session_contents = {id_token=true, access_token=true},
    introspection_cache_ignore = true
}
local res, err, target, session = require("resty.openidc").authenticate(opts, nil, "deny")
session:destroy()
return ngx.redirect("/")
