#!/bin/bash

mvn -f /opt/keycloak_logging/pom.xml clean package
ln -s /opt/keycloak_logging/target/tuency-keycloak-logging-extension.jar $KEYCLOAK_HOME/standalone/deployments
