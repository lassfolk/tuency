#!/bin/bash

set -eu -o pipefail
test -n "${DEBUG-}" && set -x

cd /opt/tuency_client/
sed -i 's/disableHostCheck: false/disableHostCheck: true/g' vue.config.js
/usr/local/openresty/nginx/sbin/nginx
yarn
yarn serve --host 0.0.0.0 --port 7080
