#!/bin/bash

set -eu -o pipefail
test -n "${DEBUG-}" && set -x

echo "### Running composer"
composer install
echo "### Running migrations"
php artisan migrate:fresh --seed
chown -R www-data /opt/tuency_backend/storage/

echo "### Starting php and nginx"
service php8.2-fpm start
nginx -g 'daemon off;'
