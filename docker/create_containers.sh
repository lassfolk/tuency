#Create a network
docker network create tuency_network

#Build images
docker build -f docker/database/Dockerfile -t tuency/database .
docker build -f docker/backend/Dockerfile -t tuency/backend .
docker build -f docker/client/Dockerfile -t tuency/client .
docker build -f docker/keycloak/Dockerfile -t tuency/keycloak .

#Run containers
docker run --name tuency_database \
    -v $PWD/docker/database:/opt/tuency_database \
    -v $PWD/docker/db:/docker-entrypoint-initdb.d \
    --net=tuency_network \
    -p 7080:7080
    -d tuency/database
docker run --name tuency_keycloak \
    --net=tuency_network \
    -p 8080:8080 \
    -d tuency/keycloak
docker run --name tuency_backend \
    --env-file ./docker/backend/dot.env \
    -v $PWD/backend:/opt/tuency_backend \
    -v $PWD/docker/backend:/opt/tuency_conf \
    --net=tuency_network \
    -p 20080:80 \
    -d tuency/backend
docker run --name tuency_client \
    -v $PWD/docs/examples/tuency_resources:/opt/tuency_resources \
    -v $PWD/client:/opt/tuency_client \
    -v $PWD/docker/client:/opt/tuency_conf \
    --net=tuency_network \
    -p 7080:80 \
    -d tuency/client

docker exec tuency_backend bash -c 'php artisan migrate:fresh --seed && php artisan config:cache'

echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_database)" "tuency_database" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_client)" "tuency_client" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_backend)" "tuency_backend" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_keycloak)" "tuency_keycloak" >> /etc/hosts
