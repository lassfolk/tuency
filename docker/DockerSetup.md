# Docker Setup

In the first step, create the container with one of the three methods.
Afterwards, access and test the containers and adapt the configuration to your local network and conditions.

## Create the containers
You have three options to create and start the containers
- using docker-compose for testing and production (**recommended**)
  - for development purposes, a docker-compose development setup is available.
- development Setup with Docker, running the commands manually step-by-step
- with plain docker too, but using the automated script

### Docker-Compose (recommended)

```bash
#Go to project root directory
cd /path/to/project/tuency
docker-compose up --build
```

Continue with the section "Access and Setup" below.

#### Docker-Compose development setup (beta)

For development purposes, use the `docker-compose.dev.yml` file.
It adds the code and configuration as docker volumes for easier development.
**But**: This is in beta state (not much tested).

### Development Setup with Docker

To create a dockerized development environment, the following can be used:

```bash
#Go to project root directory
cd /path/to/project/tuency

#Create a network
docker network create tuency_network

#Build images
docker build -f docker/database/Dockerfile -t tuency/database .
docker build -f docker/backend/Dockerfile -t tuency/backend .
docker build -f docker/client/Dockerfile -t tuency/client .
docker build -f docker/keycloak/Dockerfile -t tuency/keycloak .

#Run containers
docker run --name tuency_database -v $PWD/docker/database:/opt/tuency_database -v $PWD/docker/db:/docker-entrypoint-initdb.d --net=tuency_network -d tuency/database
docker run --name tuency_keycloak --net=tuency_network -p 28080:8080 -d tuency/keycloak
docker run --name tuency_backend -v $PWD/backend:/opt/tuency_backend -v $PWD/docker/backend:/opt/tuency_conf --net=tuency_network -p 20080:80 -d tuency/backend
docker run --name tuency_client -v $PWD/docs/examples/tuency_resources:/opt/tuency_resources -v $PWD/client:/opt/tuency_client -v $PWD/docker/client:/opt/tuency_conf --net=tuency_network -p 7080:80 -d tuency/client
```

For easier access, the container hostnames can be added to the host machine's host file:

```bash
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_database)" "tuency_database" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_client)" "tuency_client" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_backend)" "tuency_backend" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_keycloak)" "tuency_keycloak" >> /etc/hosts
```

The database can be migrated using the php tool in the backend container:
```bash
docker exec -ti tuency_backend bash
php artisan migrate:fresh --seed
```
Loading the env file is essential here, otherwise the migration won't work.

To finish the setup, the KeyCloak client needs to be imported via the KeyCloak web interface reachable under http://tuency_keycloak:8080/auth/admin/master/console/ using the username `admin` and the password `secret`:

* Navigate to `clients`
* Create a new client
* Import `docker/keycloak/tuency_client.json`
* Repeat using `docker/keycloak/tuencyone.json`, `docker/keycloak/tuencytwo.json` and `docker/keycloak/tuencythree.json`
* The imported clients include all necessary settings except client roles. These have to be added using the _roles_ tab in the respective client's settings.
 * Necessary roles:
   * portaladmin
* Assign the portaladmin role to the admin user to access the application:
 * Navigate to `Users`
 * Search for user `admin` or click "View all users" if user `admin` is not yet visible.
 * Edit admin user
 * Navigate to `Role Mappings` and assign the portaladmin for all clients using the `Client Roles` combobox
* Log out

### Using the docker scripts
```bash
./docker/create_containers.sh
```

Removing the containers:
```bash
./docker/remove_containers.sh
```

Continue with the keycloak-setup steps of the previous section.

## Access and Final Setup

After all containers are successfully up, you can check with
```bash
$ docker ps
35140a584b8e        tuency/client       "sh /opt/tuency_conf…"   About an hour ago   Up About an hour    8080/tcp, 0.0.0.0:7080->80/tcp   tuency_client
8eb0ded59c80        tuency/backend      "sh /opt/tuency_conf…"   About an hour ago   Up About an hour    0.0.0.0:20080->80/tcp            tuency_backend
e29a56c2858b        tuency/keycloak     "sh /opt/keycloak_co…"   About an hour ago   Up About an hour    0.0.0.0:8080->8080/tcp           tuency_keycloak
6bb3b5ae7cec        tuency/database      "/usr/lib/postgresql…"  About an hour ago   Up About an hour                                     tuency_database
```

For a test using multiple portals, add
```
127.0.0.1 tuencyone tuencytwo tuencythree
```
to the hosts file.

For the authentication redirects to work the Keycloak needs to be reachable under the url `tuency_keycloak`.
This can be achieved using the steps described in [TestMultipleHosts.md](../docs/TestMultipleHosts.md) or by adding the container IP address to the hosts file:

```bash
echo "$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' tuency-keycloak)" "tuency_keycloak" >> /etc/hosts
```

The applications can the be accessed using http://tuencyone:7080 and http://tuencytwo:7080.

To finish the setup of KeyCloak and configure email-delivery and themes, continue with https://gitlab.com/intevation/tuency/tuency/-/blob/master/docs/KeycloakSetup.md#user-content-activate-email-verification-and-passwort-reset
