# NEWS

## 1.1.0 (2021-07-28)

### Upgrading from 1.0.0

General upgrade instructions are in the various `README.md` files.

#### PHP backend

##### Dependencies

The `scito/keycloak-admin` has been updated to a new revision of the
`dev-execute-actions-email` branch, so a composer `install` is required.

##### Configuration

###### Auditlog

Tuency now logs requests and, optionally, changes. Both are written to
the log channel `auditlog` which can be configured in
`backend/config/logging.php`. Changes are also logged into a database
table.

By default no changes are logged. Which changes are to be logged and in
what details can be configured in `backend/config/tuency.php` where the
`auditlog` setting determines the tables and columns whose changes
should be logged.

Tuency also records which user last change a row in the database in many
tables. This can be switched on/off with the `set_updated_by` settging
in `backend/config/tuency.php`.

There's a related extension for Keycloak, see below.

##### Database

This upgrade has several database migrations, two of which may need some
manual action:

###### User information

How user information is stored in the PostgreSQL database has changed
and while the migration takes care of most of the required changes,
there's one aspect it cannot handle: Users that are in Keycloak but have
never been added to the PostgreSQL database need to be added manually.
This can be done with the new artison console command
`tuency:compare-users`. Run without arguments to list the users which are
in PostgreSQL or in Keycloak but no the other:

```sh
php artisan tuency:compare-users
```

Run it once with the `--add-to-db` option to add the users that are in
Keycloak but not yet the PostgreSQL database to the PostgreSQL database:

```sh
php artisan tuency:compare-users --add-to-db
```

###### FQDN constraints

The database has an additional constraint on FQDNs. The FQDNs must now
be lower case. The migration converts existing data but after conversion
the already existing uniqueness constraint might be violated. A typical
error message when this happens is

    duplicate key value violates unique constraint "fqdn_organisation_id_fqdn_unique"

To determine whether your database is affected or to determine which
tuples violate the constraint, you can use:

```sql
SELECT array_agg(ROW(fqdn, fqdn_id)) FROM fqdn
 GROUP BY lower(fqdn), organisation_id
HAVING count(*) > 1;
```

The easiest way to resolve this is to delete one of the duplicate FQDNs.
Since tuency is not used in production yet, deleting items should not be
much of a problem. Deleting them using the web-interface is probably
best, because that way any related information will automatically be
deleted as well.


#### Client

##### Dependencies

Many dependencies have been updated.


#### Keycloak

There's a new extension for Keycloak in `keycloak_logging/` that handles
the Keycloak part of the audit log. See the `README.md` in that
subdirectory for details.


#### Updating running docker deployment

Due to changes to the backend start script, an update of running backend containers is required.
To upgrade the script in a running container, e.g. `tuency_backend`, use the following commands:

```bash
cd /path/to/tuency/repo/
docker cp deployment/backend/start.sh tuency_backend:/usr/src/tuency-installer/
```

Additionally a keycloak logging extension has been added which needs to be deployed into the keycloak container.
To add the extension to a keycloak container, e.g. tuency_keycloak, use the following commands:

```bash
cd /path/to/tuency/repo/
docker cp keycloak_logging tuency_keycloak:/usr/src/tuency-logging
docker exec -ti tuency_keycloak bash
cd /usr/src/tuency-logging
mvn clean package
cp target/tuency-keycloak-logging-extension-jar-with-dependencies.jar /opt/keycloak-11.0.3/standalone/deployments/
```

The extension should then be loaded automatically.
For further configuration, see the [logging extension readme file](keycloak_logging/README.md).



## 1.0.0 (2021-06-17)

### Upgrading from 0.9.0

General upgrade instructions are in the various `README.md` under
`deployment/`.


#### PHP backend

##### New dependency

The backend now requires the PHP module bcmath (Debian package:
php-bcmath)

The Docker files have been updated.

##### Database

This update contains some database migrations, none of which modify or
delete existing data, so applying them should not be a problem.

##### Email

The PHP backend will now send mails in some cases. This requires some
configuration, which is described in the section *Mail Configuration* in
`backend/README.md`. See also *Customizing email notifications* in
`deployment/backend/README.md`.


#### RIPE Importer

The RIPE importer has to be updated and invoked with the right options
to support tuency fully. See the section *RIPE Data* in
`backend/README.md`.


#### nginx configuration

The files used with nginx have been updated. Particularly noteworthy are
the following files:

 - `deployment/webserver/authenticate.lua`
 - `deployment/webserver/check_auth.lua`
 - `deployment/webserver/nginx.conf`
 - `deployment/webserver/tuency_server_block`

The docker files have been updated. See section *Update* in
`deployment/webserver/README.md`.
