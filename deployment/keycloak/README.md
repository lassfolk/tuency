# KeyCloak

This folder contains a setup script to install the necessary dependencies and install KeyCloak to a given directory.

## Setup

Before running the setup script, the KeyCloak API extension needs to be compiled using maven:

```bash
#Install maven, e.g. on an Debian or Ubuntu system
sudo apt update && sudo apt install -y maven

cd /path/to/tuency/repo/keycloak_api
maven clean package
```

After successfully compiling the API extension jar, the setup script can be used. For usage information use:

```bash
cd /path/to/tuency/repo/deployment/keycloak
./setup.sh -h
# Usage: ./setup.sh --api-extension API_EXTENSION [-y] [-h] [--install-dir INSTALL_DIR] [--theme-dir THEME_DIR]
#   --api-extension Api extension jar file, mandatory
#   -y Assume yes for all prompts and run non-interactively
#   -h Show this usage information
#   --install-dir Directory to install keycloak to. Defaults to /opt.  Note: a KeyCloak subdirectory will be created
#   --theme-dir Directory containing the KeyCloak theme, defaults to ../../keycloak_theme/tuency_kc_theme
```

To run the setup use:

```bash
cd /path/to/tuency/repo/deployment/keycloak
./setup.sh --api-extension /path/to/compiled/api/extension/keycloak-api-extension.jar \
           --install-dir /keycloak/install/dir \
           --theme-dir /path/to/tuency/repo/keycloak_theme/tuency_kc_theme
```

## Further Steps

### Start KeyCloak

Start KeyCloak using standalone script

```bash
cd /keycloak/install/dir
bin/standalone.sh
```

Additionally ip addresses can be set to which the KeyCloak sockets should be bound. E.g.:

```bash
cd /keycloak/install/dir
bin/standalone.sh -b 0.0.0.0 -bmanagement=0.0.0.0
```

### Add initial admin user

An initial admin user can be created using the add user script

```bash
cd /keycloak/install/dir
bin/add-user-keycloak.sh -u myUserName
```

### Add tuency realm and client

Using the KeyCloak web application reachable under port 8080 a new realm called 'tuency' must be added.
In this realm a new client for the tuency application should be created.
This client must contain the application roles `portaladmin` and `tenantadmin`.
To apply the tuency keycloak theme go to the realm settings and select the tuency theme for account and login.

## Update
To update the keycloak, please refer to the [KeyCloak Documentation](https://www.keycloak.org/docs/latest/).

## Docker Setup

The [Dockerfile](Dockerfile) can be used to set up a KeyCloak using docker:

Build image:

```bash
cd /path/to/tuency/repo
docker build -t tuency/keycloak -f deployment/keycloak/Dockerfile .
```

Run container:

```bash
docker run --name tuency_keycloak -d tuency/keycloak
```
