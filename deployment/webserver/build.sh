#!/bin/bash
#Build the tuency application

export DEBIAN_FRONTEND=noninteractive
PARAMETER_Y=0

YARN_REPO=https://dl.yarnpkg.com/debian/

PROJECT_ROOT=$PWD/../..
CLIENT_DIR="$PROJECT_ROOT/client"
INSTALL_DIR=/usr/local/tuency

printUsage()
{
    echo "Usage: $0 [-y] [-h] [--client-dir CLIENT_DIR] [--install-dir INSTALL_DIR]"
    echo -e "\t-y Assume yes for all prompts and run non-interactively"
    echo -e "\t-h Show this usage information"
    echo -e "\t--client-dir Set the directory the tuency client application is present, defaults to ../../client"
    echo -e "\t--install-dir Set the directory the tuency application will be installed to, defaults to /usr/local/tuency"
    exit 1 # Exit script after printing help
}

options=$(getopt -o yh --long client-dir: --long install-dir: -- "$@")
[ $? -eq 0 ] || {
    printUsage
}

eval set -- "$options"

while true; do
    case "$1" in
    -y) PARAMETER_Y=1 ;;
    -h) printUsage ;;
    --client-dir)
        shift;
        CLIENT_DIR=$1
        ;;
    --install-dir)
        shift;
        INSTALL_DIR=$1
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

if [ "$EUID" -ne 0 ]
  then echo "The setup needs root privileges to install the client dependencies. Please run as root."
  exit 1
fi

DEPENDENCIES="ca-certificates curl git gnupg wget"

echo "Updating package information..."
apt-get update > /dev/null

echo "Installing backend dependencies.."

if [ "$PARAMETER_Y" -ne 0 ]
    then
    echo "The following packages will be installed: $DEPENDENCIES"
    apt-get install $DEPENDENCIES -y -qq > /dev/null

    else
    apt-get install $DEPENDENCIES
fi

APT_RESULT=$?

if [ "$APT_RESULT" -ne 0 ]
    then echo "Error: Dependency setup failed"
    exit 1
fi


echo "Installing yarn..."

echo "Adding yarn repository: $YARN_REPO"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb $YARN_REPO stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update > /dev/null

if [ "$PARAMETER_Y" -ne 0 ]
    then
    apt-get install yarn -y -qq > /dev/null
    else
    apt-get install yarn
fi

APT_RESULT=$?

if [ "$APT_RESULT" -ne 0 ]
    then echo "Error: Yarn setup failed"
    exit 1
fi

echo "Using client dir: $CLIENT_DIR"

mkdir -p $INSTALL_DIR
cd $CLIENT_DIR

echo "Building application in $CLIENT_DIR"
yarn > /dev/null
yarn build > /dev/null 2>&1
BUILD_RESULT=$?

if [ "$BUILD_RESULT" -ne 0 ]
    then echo "Error: Application build failed"
    exit 1
fi

echo "Deploying application to $INSTALL_DIR"
cp -r $CLIENT_DIR/dist/* $INSTALL_DIR/
mkdir $INSTALL_DIR/resources
