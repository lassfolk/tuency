<?php

return [
    'keycloak_admin_tuency_client_id' => env('KEYCLOAK_ADMIN_TUENCY_CLIENT_ID'),
    'keycloak_admin_tuency_realm' => env('KEYCLOAK_ADMIN_TUENCY_REALM'),
    'keycloak_url' => env('KEYCLOAK_ADMIN_URL'),

    /**
     * API token for queries by IntelMQ
     *
     * FIXME: The tokens should be handled in some other way, e.g. they should
     * be stored in a database table and use some form of hashing, etc.
     *
     * Use a good random number generator to generate the value. E.g.
     *
     *  head -c 48 /dev/urandom | base64
     */
    'intelmq_query_token' => env('INTELMQ_QUERY_TOKEN'),

    'mail_contact_expire_interval' => env('MAIL_CONTACT_EXPIRE_INTERVAL'),

    /**
     * Whether to automatically set the updated_by column of models that use
     * the HasUpdatedBy trait.
     *
     * The main reason this is configurable is that this allows us to switch
     * it off in the database seeder.
     */
    'set_updated_by' => true,

    /**
     * Auditlog
     *
     * Tuency can log all changes (create, update, delete) that are performed
     * via Eloquent. This includes the obvious tables like e.g. organisation
     * but also pivot tables, e.g. organisation_user. The changes are logged
     * to the 'auditlog' logging channel (see logging.php for how this is set
     * up) as well as the changelog table in the database.
     *
     * The auditlog setting determines which tables and columns are logged.
     * It's a mapping from table names to arrays that specify which columns
     * tuency should log in that table. In those arrays, each key is a column
     * name and the value describes whether and how the value should be
     * logged. Supported values:
     *
     *   false     Do not log
     *   true      Log the value without further processing
     *
     * There may be other values in the future, like e.g. base64 encoding.
     *
     * Changes to tables that are not included are not logged at all.
     *
     * The easiest way to determine which tables and columns can be logged is
     * in the database by inpecting the table definitions with psql's \d
     * command.
     */
    'auditlog' => [
    ],

    /**
     * Add contact fields
     *
     * If fields are to be added that are not files (PDF), the fields must be
     * added in the DB in the table 'contacts'.
     *
     * In the array 'contact_fields' the contact fields are configured as
     * follows:
     *
     * 'contact_fields' => [
     *      'name_of_the_field' => [
     *          'type' => 'Field type', // name_of_the_field must be the name as in the DB.
     *          'label' => 'Label for the field', // What to display as the label for the field
     *          'hint' => 'A hint text', // Hint to be displayed under the field. (optional)
     *      ],
     *      'a_dropdow_field' => [
     *          'label' => 'Label for the field', // What to display as the label for the field
     *          'dropdown' => [
     *               'value1' => 'Label for the value1', // The key (value1) is what is stored in the DB
     *               'wert2' => 'Label for wert2', # The value is what is displayed
     *          ],
     *          'type' => 'Field Type', # The type must be the type of the key
     *      ],
     * ]
     *
     * The following types can be specified:
     *  - text : keine Textfeld
     *  - text_area : Großes Textfeld
     *  - phone : Telefohnnummer (string)
     *  - integer : integer
     *  - s_mime_cert : s/mime certificate
     *  - openpgp_pubkey : openpgp public key
     *  - pdf : pdf Datei
     */

    'contact_fields' => [
        'first_name' => [
            'label' => 'First Name',
            'type' => 'text',
        ],
        'last_name' => [
            'label' => 'Last Name',
            'type' => 'text',
        ],
        'label' => [
            'label' => 'label',
            'type' => 'text',
        ],
        'email' => [
            'label' => 'Email',
            'type' => 'text',
        ],
        'phone_1' => [
            'label' => 'Phone 1',
            'hint' => 'Phone should match this format without spaces: +<country code><prefix><number>',
            'type' => 'phone',
        ],
        'phone_2' => [
            'label' => 'Phone 2',
            'hint' => 'Phone should match this format without spaces: +<country code><prefix><number>',
            'type' => 'phone',
        ],
        'phone_alarm_mobil' => [
            'label' => 'Phone Alarm Mobil',
            'hint' => 'Phone should match this format without spaces: +<country code><prefix><number>',
            'type' => 'phone',
        ],
        'phone_24_7' => [
            'label' => 'Phone 24/7',
            'hint' => 'Phone should match this format without spaces: +<country code><prefix><number>',
            'type' => 'phone',
        ],
        'street' => [
            'label' => 'Street',
            'type' => 'text',
        ],
        'zip' => [
            'label' => 'ZIP',
            'type' => 'integer',
        ],
        'location' => [
            'label' => 'Location',
            'type' => 'text',
        ],
        'country' => [
            'label' => 'Country',
            'type' => 'text',
            'dropdown' => [
                'de' => 'Germany',
                'at' => 'Austria',
                'ch' => 'Switzerland',
            ],
        ],
        'endpoint' => [
            'label' => 'Endpoint',
            'type' => 'text',
        ],
        's_mime_cert' => [
            'label' => 'S/MIME Cert',
            'type' => 's_mime_cert',
        ],
        'openpgp_pubkey' => [
            'label' => 'OpenPGP Pubkey',
            'type' => 'openpgp_pubkey',
        ],
        'atc_code_of_conduct' => [
            'label' => 'ATC Code of Conduct',
            'type' => 'pdf',
        ],
        'aec_code_of_conduct' => [
            'label' => 'AEC Code of Conduct',
            'type' => 'pdf',
        ],
        'document' => [
            'label' => 'Document',
            'type' => 'pdf',
        ],
        'commentary' => [
            'label' => 'Commentary',
            'type' => 'text_area',
        ],
        'format' => [
            'label' => 'Format',
            'type' => 'text',
            'dropdown' => [
                'dense_csv' => 'Dense CSV',
                'full_csv' => 'Full CSV',
                'json' => 'JSON',
            ],
        ],
    ],

    /**
     * Add contact roles
     *
     * The contact roles are configured in the array 'contact_roles'.
     *
     * The name of the role is given as the key. The value is an array of
     * contact fields that the role should have as a key and the value is true
     * or false to determine if the field is required. (true = required)
     *
     * 'contact_roles' => [
     *      'First contact role' => [
     *          'last_name' => true,
     *          'first_name' => false,
     *      ],
     *      'Second contact role' => [
     *          'email' => true,
     *          'openpgp_pubkey' => false,
     *      ],
     * ]
     */

    'contact_roles' => [
        'Ansprechpartner Primaer' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => true,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => true,
            'document' => false,
            'commentary' => false,
        ],
        'Administrator Organisation' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Ansprechpartner Stellvertreter' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => true,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => true,
            'document' => false,
            'commentary' => false,
        ],
        'Single Point of Contact (SpoC)' => [
            'label' => true,
            'email' => true,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => true,
            'document' => false,
            'commentary' => false,
        ],
        '24/7' => [
            'label' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'phone_24_7' => true,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => true,
            'document' => false,
            'commentary' => false,
        ],
        'Krisenstab' => [
            'first_name' => false,
            'last_name' => false,
            'label' => false,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'ICS Security Technik' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'ICS Security Organisation' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'IT Security Technik' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'IT Security Organisation' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Abuse Contact' => [
            'label' => true,
            'email' => true,
            'endpoint' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'commentary' => false,
            'format' => true,
        ],
        'SMS Alarmierung' => [
            'first_name' => true,
            'last_name' => true,
            'phone_alarm_mobil' => true,
            'commentary' => false,
        ],
        'Mitarbeiter Single Point of Contact (SpoC)' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Mitarbeiter 24/7' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'aec_code_of_conduct' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Plenar Einladungen' => [
            'first_name' => true,
            'last_name' => true,
            'email' => true,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'document' => false,
            'commentary' => false,
        ],
        'NIS-Kontaktstelle' => [
            'label' => true,
            'email' => true,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'document' => false,
            'commentary' => false,
        ],
        'MISP Account' => [
            'first_name' => false,
            'last_name' => false,
            'label' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'commentary' => false,
        ],
        'XYZ Member' => [
            'first_name' => true,
            'last_name' => true,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'atc_code_of_conduct' => true,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'document' => false,
            'commentary' => false,
        ],
        'Generic' => [
            'first_name' => false,
            'last_name' => false,
            'label' => false,
            'email' => false,
            'phone_1' => false,
            'phone_2' => false,
            'street' => false,
            'zip' => false,
            'location' => false,
            'country' => false,
            's_mime_cert' => false,
            'openpgp_pubkey' => false,
            'document' => false,
            'commentary' => false,
            'format' => false,
        ],
    ],
];
