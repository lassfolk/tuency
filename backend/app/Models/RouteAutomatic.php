<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RouteAutomatic extends Model
{
    protected $table = 'route_automatic';

    protected $primaryKey = 'route_automatic_id';

    // The tables managed by the RIPE importer have timestamps that are
    // different from the ones laravel expects.
    public $timestamps = false;

    public function manualOrganisations()
    {
        return $this->belongsToMany(
            Organisation::class,
            // Name of the table linking routes to manual organisations
            'asn',
            // Attribute of that table that refers to the route
            'asn',
            // Attribute of that table that refers to the organisation
            'organisation_id',
            // Attribute on the route table that is referenced by the ASN
            // table.
            'asn',
        )->wherePivot('approval', 'approved');
    }
}
