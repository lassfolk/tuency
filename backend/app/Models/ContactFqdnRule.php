<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Custom pivot model for the Contact <-> FqdnRule relationship
 *
 * The purpose of the model is to automatically update the timestamps on the
 * FqdnRule model when a contact is attached or detached to/from the
 * FQDN rule.
 */
class ContactFqdnRule extends Pivot
{
    use LogsChanges;

    protected $touches = ['fqdnRule'];

    public function fqdnRule()
    {
        return $this->belongsTo(FqdnRule::class, 'fqdn_rule_id');
    }
}
