<?php

namespace App\Models;

use App\Models\Tenant;
use App\Models\Organisation;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasUpdatedBy;
    use LogsChanges;

    protected $table = 'user';

    protected $primaryKey = 'keycloak_user_id';

    // The primary key is a string
    public $incrementing = false;
    protected $keyType = 'string';

    protected $fillable = ['keycloak_user_id'];

    protected static function booted()
    {
        static::deleting(function ($user) {
            // Remove links to tenants and tags
            $user->tenants()->detach();
            $user->organisations()->detach();
        });
    }

    /**
     * A user can belong to many tenants.
     */
    public function tenants()
    {
        return $this->belongsToMany(
            Tenant::class,
            'tenant_user',
            'keycloak_user_id',
            'tenant_id'
        )->using(TenantUser::class)->withTimestamps();
    }

    /**
     * A user can belong to many organisations.
     */
    public function organisations()
    {
        return $this->belongsToMany(
            Organisation::class,
            'organisation_user',
            'keycloak_user_id',
            'organisation_id'
        )->using(OrganisationUser::class)->withTimestamps();
    }
}
