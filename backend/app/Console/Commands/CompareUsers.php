<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Console\Commands;

use App\Console\Commands\AccessesKeycloak;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Scito\Laravel\Keycloak\Admin\Facades\KeycloakAdmin;

class CompareUsers extends Command
{
    use AccessesKeycloak;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tuency:compare-users
                            {--add-to-db : Add users to the database that are only known to Keycloak}
                            {--delete-from-db : Delete users from the database that are not known to Keycloak}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compare user information in the database with Keycloak';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $realm = $this->openRealm();
        $result = [];
        $users = $realm->users();

        $keycloakUsers = collect($users->search()->get())
            ->keyBy(function ($user) {
                return $user->getId();
            });

        $tuencyUsers = User::select('keycloak_user_id')->get()
            ->keyBy(function ($user) {
                return $user->keycloak_user_id;
            });

        $notInDB = $keycloakUsers->diffKeys($tuencyUsers);
        $notInKeycloak = $tuencyUsers->diffKeys($keycloakUsers);

        if ($notInDB->isNotEmpty()) {
            $this->info('Users in Keycloak but not in the database');
            $this->table(
                ['Keycloak UUID', 'username'],
                $notInDB->map(function ($user) {
                    return [$user->getId(), $user->getUsername()];
                })->sortKeys()->toArray()
            );
        } else {
            $this->info('All Keycloak users are in the database');
        }
        $this->newline();

        if ($notInKeycloak->isNotEmpty()) {
            $this->info('Users in the database but not in Keycloak');
            $this->table(
                ['Keycloak UUID'],
                $notInKeycloak->map(function ($user) {
                    return [$user->keycloak_user_id];
                })->sortKeys()->toArray()
            );
        } else {
            $this->info('All users in the database are also in Keycloak');
        }

        if ($this->option('delete-from-db')) {
            $this->newline();
            $this->info("Deleting unknown users from database:");
            $notInKeycloak->each(function ($item, $key) {
                // Bypass Eloquent because going through Eloquent would likely
                // fail because this script does not run as a normal
                // authenticated tuency user and therefore some event handles
                // that log changes will not work.
                $id = $item->keycloak_user_id;
                $this->info("Deleting $id");
                DB::delete('DELETE FROM "user" WHERE keycloak_user_id = ?', [$id]);
            });
        }

        if ($this->option('add-to-db')) {
            $this->newline();
            $this->info("Adding unknown users to the database:");
            $notInDB->each(function ($item, $key) {
                // Bypass Eloquent because going through Eloquent would likely
                // fail because this script does not run as a normal
                // authenticated tuency user and therefore some event handles
                // that log changes will not work.
                $id = $item->getId();
                $this->info("Adding $id");
                DB::insert('INSERT INTO "user" (keycloak_user_id) VALUES (?)', [$id]);
            });
        }

        return 0;
    }
}
