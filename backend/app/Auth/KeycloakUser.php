<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Authors:
 * * 2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 * * 2020 Bernhard Reiter <bernhard.reiter@intevation.de>
 */

namespace App\Auth;

use App\Models\Organisation;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Support\Facades\DB;

/**
 * Custom user class for KeyCloak based authorization in tuency.
 */
class KeycloakUser implements AuthorizableContract
{
    use Authorizable;

    private $keycloakUserId;
    private $username;
    private $groups;
    private $clientId;
    private $fullname;

    public function __construct(
        string $keycloakUserId,
        string $username,
        array $groups,
        string $clientId,
        ?string $fullname
    ) {
        $this->keycloakUserId = $keycloakUserId;
        $this->username = $username;
        $this->groups = $groups;
        $this->clientId = $clientId;
        $this->fullname = $fullname;
    }


    public function getKeycloakUserId()
    {
        return $this->keycloakUserId;
    }

    public function getGroups()
    {
        return $this->groups;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Return whether the user is a portal admin.
     */
    public function isPortalAdmin()
    {
        return in_array('portaladmin', $this->groups);
    }

    /**
     * Return whether the user is a tenant admin.
     */
    public function isTenantAdmin()
    {
        return in_array('tenantadmin', $this->groups);
    }

    /**
     * Return whether the user is an organisation admin.
     */
    public function isOrgaAdmin()
    {
        return in_array('orgaadmin', $this->groups);
    }

    /**
     * Return details about the groups the user belongs to.
     *
     * This method performs database queries where necessary, so is not a
     * simple and cheap property accessor.
     *
     * The return value is an array with one element for each group. Each
     * group is an array with the following keys:
     *
     *  'level': The group level, one of 'portaladmin', 'tenantadmin',
     *           'orgaadmin'
     *
     *  'nodes': An array of the nodes the user is associated with at this
     *           level. Only present for 'tenantadmin' and 'orgaadmin'. Each
     *           element is an array with the following keys:
     *
     *            'name': The name of the node (e.g. organisation name)
     *            'node_id': The ID of the node (e.g. the organisation id)
     *
     *           A node describing an organisation has two additional keys:
     *
     *            'parents': An array with the names of the parent
     *                       organisations.
     *            'tenants': An array with the names of the tenants the
     *                       organisation belongs to.
     *
     * The organisations listed for an orgaadmin are those the user is
     * directly associated with. In particular, it does not include their
     * sub-organisations.
     *
     * Example result in JSON:
     *
     *     [
     *         {
     *             "level" : "orgaadmin",
     *             "nodes" : [
     *                 {
     *                     "node_id" : 1,
     *                     "name" : "An Organisation",
     *                     "parents" : [
     *                         "Parent Organisation"
     *                     ],
     *                     "tenants" : [
     *                         "Tenant A"
     *                     ]
     *                 }
     *             ]
     *         }
     *     ]
     */
    public function resolveGroups()
    {
        $groups = [];
        if ($this->isPortalAdmin()) {
            $groups[] = [
                'level' => 'portaladmin',
            ];
        }
        if ($this->isTenantAdmin()) {
            $tenants = DB::table('tenant_user')
                ->join('tenant', 'tenant.tenant_id', '=', 'tenant_user.tenant_id')
                ->where('keycloak_user_id', $this->getKeycloakUserId())
                ->select('tenant.tenant_id as node_id', 'tenant.name as name')
                ->get();
            $groups[] = [
                'level' => 'tenantadmin',
                'nodes' => $tenants,
            ];
        }
        if ($this->isOrgaAdmin()) {
            $organisations = Organisation::orgaAdmin($this->getKeycloakUserId())
                ->with('tenants:name')
                ->get();

            $groups[] = [
                'level' => 'orgaadmin',
                'nodes' => $organisations->map(function ($o) {
                    return [
                        'node_id' => $o->organisation_id,
                        'name' => $o->name,
                        'parents' => Organisation::getParents($o->organisation_id),
                        'tenants' => $o->tenants->map(function ($t) {
                            return ['name' => $t['name']];
                        }),
                    ];
                }),
            ];
        }
        return $groups;
    }
}
