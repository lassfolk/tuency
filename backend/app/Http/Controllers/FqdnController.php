<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Fqdn;
use App\Models\Organisation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class FqdnController extends Controller
{
    use HandlesClaims;

    /**
     * List the fqdns associated with an organisation
     */
    public function index(Organisation $organisation)
    {
        $this->logRequest();
        return $organisation->fqdns;
    }

    /**
     * Create a new fqdn associated with an organisation.
     */
    public function store(Request $request, Organisation $organisation)
    {
        $validator = Validator::make($request->all(), [
            'fqdn' => [
                'string',
                'required',
                function ($attribute, $value, $fail) use ($organisation) {
                    $collisions = Fqdn::where('fqdn', '=', strtolower($value))
                        ->whereHas('organisation', function ($query) use ($organisation) {
                            $query->whereKey($organisation);
                        })
                        ->get();
                    if ($collisions->isNotEmpty()) {
                        $fail('The organisation already has an FQDN with that value.');
                    }
                },
            ],
            'subdomainof' => 'int|nullable'
        ])->after(function ($validator) use ($organisation) {
            $validated = $validator->validated();

            $fqdn = $validated['fqdn'];
            if (!filter_var($fqdn, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
                $validator->errors()->add('fqdn', 'invalid host name');
            }

            $parentId = $validated['subdomainof'] ?? null;

            if (!is_null($parentId)) {
                $parent = $organisation->fqdns->find($parentId);
                if (is_null($parent)) {
                    $validator->errors()->add(
                        'subdomainof',
                        'The domain given by subdomainof does not exist'
                    );
                } else {
                    if ($parent->approval !== 'approved') {
                        $validator->errors()->add(
                            'subdomainof',
                            'The domain given by subdomainof has not been approved'
                        );
                    }
                    if (!self::isSubDomain($parent->fqdn, $fqdn)) {
                        $validator->errors()->add(
                            'subdomainof',
                            'The domain given by subdomainof does not contain the domain'
                        );
                    }
                }
            }
        });

        $validated = $validator->validate();

        $this->logRequest($validated);

        $fqdn = Fqdn::create([
            'fqdn' => $validated['fqdn'],
            'organisation_id' => $organisation->organisation_id,
            'approval' => isset($validated['subdomainof']) ? 'approved' : 'pending',
        ]);

        return $fqdn;
    }

    /**
     * Check that $subdomain is a sub-domain of $domain.
     */
    public static function isSubDomain($domain, $subdomain)
    {
        $domainParts = Str::of($domain)->explode('.');
        $subDomainParts = Str::of($subdomain)->explode('.');
        $subCount = count($subDomainParts) - count($domainParts);

        if ($subCount < 0) {
            // If the sub-domain has fewer parts than the domain it cannot be
            // a sub-domain.
            return false;
        }

        return $subDomainParts->skip($subCount)->values() == $domainParts->values();
    }


    /**
     * Update a fqdn.
     *
     * This is only relevant for the approval process, so the only field that
     * can be changed is 'approval' and only tenant- and portaladmins are
     * allowed to change it.
     */
    public function update(Request $request, Organisation $organisation, Fqdn $fqdn)
    {
        return $this->updateNetObject($request, $organisation, $fqdn);
    }

    public function getConflictingNetObjects(Fqdn $fqdn)
    {
        return Fqdn::whereKeyNot($fqdn)
            ->where('organisation_id', '<>', $fqdn->organisation_id)
            ->where(function ($query) use ($fqdn) {
                return $query->where('fqdn', '=', $fqdn->fqdn)
                    ->orWhere('fqdn', 'LIKE', '%.' . $fqdn->fqdn);
            })
            ->where('approval', '=', 'approved')
            ->with('organisation')
            ->get();
    }

    /**
     * Remove a fqdn
     */
    public function destroy(Organisation $organisation, Fqdn $fqdn)
    {
        $this->logRequest();
        $fqdn->delete();
    }

    /**
     * Return claimed FQDNs that conflict with a given FQDN claim.
     *
     * Two FQDN claims conflict with one another if one contains the
     * other. For each conflicting FQDN claim this method returns the
     * address, the tenants to which the FQDN belongs and the name of the
     * organisations to which it belongs, if the requesting user belongs to
     * one of the organisation's tenants.
     */
    public function conflicts(Organisation $organisation, Fqdn $fqdn)
    {
        Gate::authorize('manage-claims');

        $this->logRequest();

        // The route does not use scoped resolution, so we have to check this
        // explicitly.
        abort_unless($fqdn->organisation_id === $organisation->getKey(), 404);

        $domainHierarchy = [];
        $domainParts = Str::of($fqdn->fqdn)->explode('.');
        for ($i = 0; $i < count($domainParts); $i++) {
            $domainHierarchy[] = $domainParts->skip($i)->join('.');
        }

        $fqdns = Fqdn::whereKeyNot($fqdn)
            ->where(function ($query) use ($fqdn, $domainHierarchy) {
                return $query->whereIn('fqdn', $domainHierarchy)
                    ->orWhere('fqdn', 'LIKE', '%.' . $fqdn->fqdn);
            })
            ->where('approval', '<>', 'denied')
            ->with('organisation.tenants')
            ->get();

        return $this->processConflicts($fqdns, 'fqdn');
    }
}
