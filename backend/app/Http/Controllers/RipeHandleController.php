<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author(s):
 * 2021 Fadi Abbud <fadi.abbud@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\RipeHandle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class RipeHandleController extends Controller
{
    use HandlesClaims;

    /**
     * List the Ripe_handles associated with an organisation
     */
    public function index(Organisation $organisation)
    {
        $this->logRequest();
        return $organisation->ripeHandles;
    }

    /**
     * Create a new Ripe_hadle associated with an organisation.
     */
    public function store(Request $request, Organisation $organisation)
    {
        $validated = $request->validate(['ripe_org_hdl' => 'string|required']);
        $this->logRequest($validated);

        $ripe_handle = RipeHandle::create([
            'ripe_org_hdl' => $validated['ripe_org_hdl'],
            'organisation_id' => $organisation->organisation_id,
            'approval' => 'pending'
        ]);

        return $ripe_handle;
    }

    public function update(Request $request, Organisation $organisation, RipeHandle $ripe_handle)
    {
        return $this->updateNetObject($request, $organisation, $ripe_handle);
    }

    public function getConflictingNetObjects(RipeHandle $ripeHandle)
    {
        return RipeHandle::whereKeyNot($ripeHandle)
            ->where('organisation_id', '<>', $ripeHandle->organisation_id)
            ->where('ripe_org_hdl', $ripeHandle->ripe_org_hdl)
            ->where('approval', '=', 'approved')
            ->with('organisation.tenants')
            ->get();
    }

    /**
     * Remove a ripe_handle
     */
    public function destroy(Organisation $organisation, RipeHandle $ripeHandle)
    {
        $this->logRequest();
        $ripeHandle->delete();
    }

    /**
     * Return claimed RIPE-handles that conflict with a given RIPE-handle claim.
     *
     * Two RIPE-handle claims conflict with one another if the refer to the
     * same RIPE-handle.
     */
    public function conflicts(Organisation $organisation, RipeHandle $ripeHandle)
    {
        Gate::authorize('manage-claims');

        $this->logRequest();

        // The route does not use scoped resolution, so we have to check this
        // explicitly.
        abort_unless($ripeHandle->organisation_id === $organisation->getKey(), 404);

        $ripeHandles = RipeHandle::whereKeyNot($ripeHandle)
            ->where('ripe_org_hdl', $ripeHandle->ripe_org_hdl)
            ->where('approval', '<>', 'denied')
            ->with('organisation.tenants')
            ->get();

        return $this->processConflicts($ripeHandles, 'ripe_org_hdl');
    }
}
