<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use IPTools\Network as IPToolsNetwork;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Organisation;
use App\Models\OrganisationAutomatic;
use App\Models\Network;
use App\Models\NetworkAutomatic;
use App\Models\Fqdn;
use App\Models\RouteAutomatic;
use App\Models\GlobalRule;

class EventDestinationController extends Controller
{
    /**
     * Lookup notification destinations for an IntelMQ event.
     *
     * For authorization, the request must contain an Authorize header field
     * with a Bearer token, e.g.
     *
     *  Authorization: Bearer 3JEba0eDP...
     *
     * The value of the token must match the configuration setting
     * tuency.intelmq_query_token.
     */
    public function lookup(Request $request)
    {
        $requestToken = $request->bearerToken();
        if (is_null($requestToken) || !hash_equals(config('tuency.intelmq_query_token'), $requestToken)) {
            abort(403);
        }

        $validated = $request->validate([
            'classification_taxonomy' => 'string|required',
            'classification_type' => 'string|required',
            'feed_provider' => 'string|required',
            'feed_name' => 'string|required',
            'feed_status' => 'string|required',
            'ip' => 'ip|required_without_all:domain',
            'domain' => 'string|required_without_all:ip',
        ]);

        $matchedRule = null;
        $result = [];

        if (isset($validated['ip'])) {
            list ($destinations, $matchedRule, $matchedNetobject) =
                $this->lookupIp($validated['ip'], $validated);
            $result['ip'] = [
                'destinations' => $destinations,
                'netobject' => $matchedNetobject,
            ];
        }

        if (isset($validated['domain'])) {
            list ($destinations, $matchedRule, $matchedNetobject) =
                $this->lookupDomain(strtolower($validated['domain']), $validated);
            $result['domain'] = [
                'destinations' => $destinations,
                'netobject' => $matchedNetobject,
            ];
        }

        if (!is_null($matchedRule)) {
            $result['suppress'] = $matchedRule->suppress;
            $result['interval'] = [
                'unit' => $matchedRule->interval_unit,
                'length' => $matchedRule->interval_length,
            ];

            $org = $matchedRule->organisation;
            if (!is_null($org)) {
                $result['constituencies'] = $org->tenants()->orderBy('name')->pluck('name');
            }
        }

        return $result;
    }

    /**
     * Find network matches
     */
    public function lookupIp($ip, $criteria)
    {
        $manualNetworks = Network::where('approval', 'approved')
            ->where('address', '>>=', $ip)
            ->with('organisation', function ($query) {
                $query->with('rules', function ($query) {
                    $query->with([
                        'contacts',
                        'classificationTaxonomy',
                        'classificationType',
                        'feedProvider',
                        'feedName',
                    ])->orderBy('organisation_rule_id');
                });
            })
            ->with('rules', function ($query) {
                $query->with([
                    'contacts',
                    'classificationTaxonomy',
                    'classificationType',
                    'feedProvider',
                    'feedName',
                ])->orderBy('network_rule_id');
            })->get();

        $automaticNetworks = NetworkAutomatic::where('address', '>>=', $ip)
                ->with('organisations')
                ->with('rules', function ($query) {
                    $query->with([
                        'contacts',
                        'classificationTaxonomy',
                        'classificationType',
                        'feedProvider',
                        'feedName',
                    ])->orderBy('network_automatic_rule_id');
                })->get();

        $routeNetworks = RouteAutomatic::where('address', '>>=', $ip)
            ->whereHas('manualOrganisations')
            ->with('manualOrganisations', function ($query) {
                $query->with('rules', function ($query) {
                    $query->with([
                        'contacts',
                        'classificationTaxonomy',
                        'classificationType',
                        'feedProvider',
                        'feedName',
                    ])->orderBy('organisation_rule_id');
                });
            })->get();

        // Postprocess data to handle sub-net relationshiops when applying
        // rules. This applies to the manual networks. The other network
        // matches are mapped to the same data structure to make further
        // processing easier.
        //
        // The case we need to handle is that an organisation may have a
        // network and one of its sub-networks and some other organisation may
        // have a network that is in between those networks. E.g.:
        //
        // Org1: 192.168.0.0/16, 192.168.130.0/24
        // Org2: 192.168.128.0/17
        //
        // A lookup of IP address 192.168.130.24 will match all of these
        // networks. When applying the rules all of the rules of Org1
        // including the ones for the /16 network and Org1 have to be
        // considered before applying the any of the rules the /17 network or
        // Org2.
        //
        // We handle this by grouping the manual networks by organisation_id
        // and then sorting the groups by the length of the longest netmask of
        // the groups networks, yielding a collection sorted most the specific
        // match first where each item is an array of network objects.
        $manualGrouped = $manualNetworks
            ->groupBy('organisation_id')
            ->map(function ($item, $key) {
                return $item->sortByDesc(function ($item, $key) {
                    return IPToolsNetwork::parse($item->address)->getPrefixLength();
                });
            });

        // Transform the other results...
        $automaticGrouped = $automaticNetworks->map(function ($item, $key) {
            return collect([$item]);
        });
        $routeGrouped = $routeNetworks->map(function ($item, $key) {
            return collect([$item]);
        });

        // and handle them as one sequence
        $groupedNetworks = $manualGrouped->concat($automaticGrouped)->concat($routeGrouped);

        // Sort the networks by specificity, meaning longer netmasks first. In
        // case there are manual and automatic network with the same length,
        // sort the manual ones first, because they can have attached rules
        // and users probably expect them to apply even if there's an
        // automatic network as well. Also, it's seems sensible to have
        // predictable ordering behavior.
        $groupedNetworks = $groupedNetworks->sortByDesc(function ($item, $key) {
            $priority = 0;
            $network = $item->first();
            if ($network instanceof RouteAutomatic) {
                $priority = 1;
            } elseif ($network instanceof Network) {
                $priority = 2;
            }
            return [
                IPToolsNetwork::parse($network->address)->getPrefixLength(),
                $priority,
            ];
        });

        $matchedRule = null;
        $destinations = [];
        $matchedAddress = null;

        foreach ($groupedNetworks as $group) {
            foreach ($group as $network) {
                foreach ($network->rules ?? [] as $rule) {
                    if ($rule->matches($criteria)) {
                        if ($network instanceof NetworkAutomatic) {
                            $organisation = $rule->organisation;
                        } else {
                            $organisation = $network->organisation;
                        }
                        $destinations[] = [
                            'source' => $network->import_source ?? 'portal',
                            'name' => $organisation->name,
                            'contacts' => $this->convertRuleContacts($rule),
                        ];
                        $matchedRule = $rule;
                        break 2;
                    }
                }
            }

            if (is_null($matchedRule)) {
                $network = $group->first();

                if ($network instanceof NetworkAutomatic) {
                    $org = $network->organisations->first();
                    if (!is_null($org)) {
                        $destinations[] = [
                            'source' => $org->import_source,
                            'name' => $org->name,
                            'contacts' => $this->convertContacts($org->contacts),
                        ];
                    }
                } else {
                    $organisations = [];
                    if ($network instanceof Network) {
                        $organisations = [$network->organisation];
                    } elseif ($network instanceof RouteAutomatic) {
                        $organisations = $network->manualOrganisations;
                    }
                    foreach ($organisations ?? [] as $org) {
                        foreach ($org->rules ?? [] as $rule) {
                            if ($rule->matches($criteria)) {
                                $destinations[] = [
                                    // Only manually maintained organisations
                                    // have rules, so the source is 'portal'
                                    // here
                                    'source' => 'portal',
                                    'name' => $org->name,
                                    'contacts' => $this->convertRuleContacts($rule),
                                ];
                                $matchedRule = $rule;
                                break 2;
                            }
                        }
                    }
                }
            }

            if (!is_null($matchedRule)) {
                $matchedAddress = $group->first()->address;
                break;
            }
        }

        if (empty($destinations)) {
            return $this->applyGlobalRules($criteria);
        }

        return array ($destinations, $matchedRule, $matchedAddress);
    }


    /**
     * Find domain matches
     */
    public function lookupDomain($domain, $criteria)
    {
        $domainHierarchy = [];
        $domainParts = Str::of($domain)->explode('.');
        for ($i = 0; $i < count($domainParts); $i++) {
            $domainHierarchy[] = $domainParts->skip($i)->join('.');
        }

        $fqdns = Fqdn::where('approval', 'approved')
            ->whereIn('fqdn', $domainHierarchy)
            ->with('organisation', function ($query) {
                $query->with('rules', function ($query) {
                    $query->with([
                        'contacts',
                        'classificationTaxonomy',
                        'classificationType',
                        'feedProvider',
                        'feedName',
                    ])->orderBy('organisation_rule_id');
                });
            })
            ->with('rules', function ($query) {
                $query->with([
                    'contacts',
                    'classificationTaxonomy',
                    'classificationType',
                    'feedProvider',
                    'feedName',
                ])->orderBy('fqdn_rule_id');
            })->get();

        // Postprocess data to handle sub-domain relationships when applying
        // rules. This is the same approach as for sub-net relationships. See
        // the more detailed comment there for details.
        $grouped = $fqdns
            ->groupBy('organisation_id')
            ->map(function ($item, $key) {
                return $item->sortByDesc(function ($item, $key) {
                    return Str::length($item->fqdn);
                });
            })->sortByDesc(function ($item, $key) {
                return Str::length($item->first()->fqdn);
            });

        $matchedRule = null;
        $destinations = [];
        $matchedFqdn = null;

        foreach ($grouped as $fqdns) {
            foreach ($fqdns as $fqdn) {
                foreach ($fqdn->rules ?? [] as $rule) {
                    if ($rule->matches($criteria)) {
                        $destinations[] = [
                            'source' => 'portal',
                            'name' => $fqdn->organisation->name,
                            'contacts' => $this->convertRuleContacts($rule),
                        ];
                        $matchedRule = $rule;
                        break 2;
                    }
                }
            }

            // If none of the rules directly associated with the FQDN have
            // matched, try the rules of the organisation the FQDN belongs to
            if (is_null($matchedRule)) {
                $fqdn = $fqdns->first();
                foreach ($fqdn->organisation->rules ?? [] as $rule) {
                    if ($rule->matches($criteria)) {
                        $destinations[] = [
                            // Only manually maintained organisations have
                            // rules, so the source is 'portal' here
                            'source' => 'portal',
                            'name' => $fqdn->organisation->name,
                            'contacts' => $this->convertRuleContacts($rule),
                        ];
                        $matchedRule = $rule;
                        break;
                    }
                }
            }

            if (!is_null($matchedRule)) {
                $matchedFqdn = $fqdns->first()->fqdn;
                break;
            }
        }

        if (empty($destinations)) {
            return $this->applyGlobalRules($criteria);
        }

        return array ($destinations, $matchedRule, $matchedFqdn);
    }

    public function applyGlobalRules($criteria)
    {
        $matchedRule = null;
        $destinations = [];

        foreach (GlobalRule::orderBy('global_rule_id')->get() as $rule) {
            if ($rule->matches($criteria)) {
                $destinations[] = [
                    'source' => 'portal',
                ];
                $matchedRule = $rule;
                break;
            }
        }

        return array ($destinations, $matchedRule, null);
    }

    public function convertContacts($contacts)
    {
        $result = [];

        foreach ($contacts as $contact) {
            $result[] = [
                'email' => $contact->email,
                'endpoint' => $contact->endpoint,
                'format' => $contact->format,
                's_mime_cert' => $contact->s_mime_cert,
                'openpgp_pubkey' => $contact->openpgp_pubkey,
            ];
        }

        return $result;
    }

    public function convertRuleContacts($rule)
    {
        $result = $this->convertContacts($rule->contacts);

        if ($rule->abuse_c) {
            $result[] = [
                'email' => $rule->preferredAbuseC(),
                'endpoint' => null,
                'format' => null,
                's_mime_cert' => null,
                'openpgp_pubkey' => null,
            ];
        }

        return $result;
    }
}
