<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Asn;
use App\Models\Contact;
use App\Models\ContactTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class ContactTagController extends Controller
{
    /**
     * List the contact tags
     */
    public function index()
    {
        $this->logRequest();
        return ContactTag::forUser(Auth::user())->with('tenants')->get();
    }

    /**
     * Create a new contact tag.
     */
    public function store(Request $request)
    {
        Gate::authorize('manage-contact-tags');

        $validated = $request->validate([
            'name' => 'string|required',
            'visible' => 'boolean|required',
            'tenants.*' => ['integer', Rule::exists('tenant', 'tenant_id')],
        ]);

        $this->logRequest($validated);

        return DB::transaction(function () use ($validated) {
            $contactTag = ContactTag::create($validated);
            if (array_key_exists('tenants', $validated)) {
                $contactTag->tenants()->sync($validated['tenants']);
            }
            return $contactTag;
        });
    }

    /**
     * Update an contact tag.
     */
    public function update(Request $request, ContactTag $contactTag)
    {
        Gate::authorize('manage-contact-tags');

        $validated = $request->validate([
            'name' => 'string',
            'visible' => 'boolean',
            'tenants.*' => ['integer', Rule::exists('tenant', 'tenant_id')],
        ]);

        $this->logRequest($validated);

        return DB::transaction(function () use ($contactTag, $validated) {
            $contactTag->update($validated);
            if (array_key_exists('tenants', $validated)) {
                $contactTag->tenants()->sync($validated['tenants']);

                // The set of tenants has changed, so some contacts may not be
                // allowed to have the tag anymore.
                $affectedContacts = Contact::forTag($contactTag->getKey())->with('tags')->get();
                foreach ($affectedContacts as $contact) {
                    $contact->removeIllegalTags();
                }
            }
            return $contactTag;
        });
    }

    /**
     * Remove an contacttag
     */
    public function destroy(Contacttag $contactTag)
    {
        Gate::authorize('manage-contact-tags');

        $this->logRequest();
        $contactTag->delete();
    }
}
