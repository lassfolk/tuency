<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Magnus Schieder <magnus.schieder@intevation.de>
 */

namespace App\Http\Controllers;

use App\Auth\KeycloakUser;
use App\Models\Organisation;
use App\Models\Contact;
use App\Models\ContactTag;
use App\Models\OrganisationTag;
use App\Models\PDF;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    private $contactRoles;
    private $contactFields;
    private $contactsFieldsValidation;
    private $dropdownFields;
    private $searchedFields;

    public function __construct()
    {
        $contactRoles = config('tuency.contact_roles');
        if ($contactRoles === null) {
            Log::error("tuency.contact_roles not set");
            abort(500);
        }

        $contactFields = config('tuency.contact_fields');
        if ($contactFields === null) {
             Log::error("tuency.contact_fields not set");
             abort(500);
        }

        $contactsFieldsValidation = [];
        $dropdownFields = [];
        $searchedFields = [];
        foreach ($contactFields as $fieldName => $properties) {
            if (array_key_exists('dropdown', $properties)) {
                $dropdownFields[$fieldName] = $properties['dropdown'];
                $contactsFieldsValidation[$fieldName] = [
                    'nullable',
                    Rule::in(array_keys($properties['dropdown'])),
                ];
            } elseif (
                $properties['type'] === 'text' ||
                $properties['type'] === 'text_area' ||
                $properties['type'] === 'phone'
            ) {
                $contactsFieldsValidation[$fieldName] = 'string|nullable';
                array_push($searchedFields, $fieldName);
            } elseif ($properties['type'] === 'integer') {
                $contactsFieldsValidation[$fieldName] = 'integer|nullable';
                array_push($searchedFields, $fieldName);
            } elseif ($properties['type'] === 's_mime_cert' || $properties['type'] === 'openpgp_pubkey') {
                $contactsFieldsValidation[$fieldName] = 'string|nullable';
            } elseif ($properties['type'] === 'pdf') {
                $contactsFieldsValidation[$fieldName] = 'file|mimes:pdf|nullable';
            }
        };

        $this->contactRoles = $contactRoles;
        $this->contactFields = $contactFields;
        $this->contactsFieldsValidation = $contactsFieldsValidation;
        $this->dropdownFields = $dropdownFields;
        $this->searchedFields = $searchedFields;
    }

    /**
     * Create a pattern for LIKE that matches a given substring.
     *
     * This method replaces '_' with '\_' and '%' with '\%' so that the
     * database does not interpret them as wild-cards and then surrounds
     * the result with '%' wild-cards so that it matches a sub-string.
     */
    private function substringPattern(string $literal)
    {
        $escaped = str_replace('_', '\\_', str_replace('%', '\\%', $literal));
        return "%$escaped%";
    }

    /*
     * Retrieve all contacts that the user is allowed to see.
     *
     * If the optional query parameter 'page' is given the result is
     * paginated using Laravel's standard pagination. The value of the
     * page parameter should be an integer. Page numbering starts from 1.
     *
     * The parameter 'sort_by' can be used to specify the field by which the
     * contacts should be sorted.
     *
     * The parameter 'row' specifies how many entries are to be displayed per
     * page. If "row" is not specified, the default value 10 is used.
     *
     * With the optional query parameter 'ancestor', which must be the ID of an
     * organisation if specified, the result is restricted to contacts from that
     * organisation and its direct and indirect sub-organisations.
     *
     * With the optional query parameter 'tenant', which must be the ID of a
     * tenant if specified, the result is restricted to contacts of
     * organisations that are connected to this tenant directly or indirectly
     * through their parents.
     */

    public function contacts(Request $request)
    {
        $validated = $request->validate([
            'page' => 'int|nullable',
            'sort_by' => 'string|nullable|in:last_name,first_name,contact_id',
            'row' => 'int|nullable',
            'ancestor' => 'int|nullable',
            'tenant' => 'int|nullable',
            'tag' => [
                'integer',
                function ($attribute, $value, $fail) {
                    if (is_null(ContactTag::forUser(Auth::user())->find($value))) {
                        $fail('Unknown contact tag');
                    }
                }
            ],
            'organisation_tag' => [
                'integer',
                function ($attribute, $value, $fail) {
                    if (is_null(OrganisationTag::forUser(Auth::user())->find($value))) {
                        $fail('Unknown organisation tag');
                    }
                }
            ],
            'search' => 'string|nullable',
            'organisation' => 'int|nullable',
        ]);

        $this->logRequest($validated);

        $page = $validated['page'] ?? -1;
        $sortBy = $validated['sort_by'] ?? "last_name";
        $row = $validated['row'] ?? 10;
        $tag = $validated['tag'] ?? -1;
        $search = $validated['search'] ?? '';

        // Determine IDs of the organisations whose contacts we want
        $orgaQuery = Organisation::querySubHierarchy(
            Auth::user(),
            $validated['ancestor'] ?? null,
            $validated['tenant'] ?? null,
        );
        if (array_key_exists('organisation', $validated)) {
            $orgaQuery = $orgaQuery->where(
                'organisation.organisation_id',
                '=',
                $validated['organisation']
            );
        }
        if (array_key_exists('organisation_tag', $validated)) {
            $orgaQuery = $orgaQuery->whereHas('tags', function ($query) use ($validated) {
                $query->whereKey($validated['organisation_tag']);
            });
        }

        // Build contact query
        $query = Contact::whereIn(
            'organisation_id',
            $orgaQuery->pluck('organisation.organisation_id')
        );

        if ($tag >= 0) {
            $query = $query->forTag($tag);
        }

        if ($search !== '') {
            $pattern = $this->substringPattern($validated['search']);

            $query = $query->where(function ($query) use ($pattern) {
                foreach ($this->searchedFields as $searchedField) {
                    $query->orWhere('contact.' . $searchedField, 'ilike', $pattern);
                }
            });
        }

        $query->with([
            'tags' => function ($query) {
                $query->forUser(Auth::user());
            },
        ]);

        if ($sortBy === "last_name") {
             $query = $query->orderBy("last_name");
             $query = $query->orderBy("first_name");
        } elseif ($sortBy === "first_name") {
             $query = $query->orderBy("first_name");
             $query = $query->orderBy("last_name");
        }
        $query = $query->orderBy("contact_id");


        // Perform query
        if ($page > 0) {
            $contacts = $query->paginate($row, ['*'], 'page', $page);
        } else {
            $contacts = $query->get();
        }

        // Postprocess results
        foreach ($contacts as $contact) {
            foreach ($this->dropdownFields as $fieldName => $dropdownOption) {
                if ($value = $contact[$fieldName]) {
                    $contact[$fieldName] = [
                        'label' => $dropdownOption[$value],
                        'value' => $value
                    ];
                } else {
                    $contact[$fieldName] = (object)[];
                }
            }

            $contact->organisation_name = $contact->organisation()->value('name');

            if ($pdfs = $contact->pdf()->get()) {
                foreach ($pdfs as $pdf) {
                    $contact[$pdf->type] = [
                        'name' => $pdf->name,
                        'id' => $pdf->pdf_id,
                    ];
                }
            }
        }

        return $contacts;
    }

    /**
     * Retrieve all contacts of the organization.
     */

    public function index(Organisation $organisation)
    {
        $this->logRequest();

        $contacts = $organisation->contacts()->with([
            'tags' => function ($query) {
                $query->forUser(Auth::user());
            },
        ])->get();

        foreach ($contacts as $contact) {
            foreach ($this->dropdownFields as $fieldName => $dropdownOption) {
                if ($value = $contact[$fieldName]) {
                    $contact[$fieldName] = [
                        'label' => $dropdownOption[$value],
                        'value' => $value
                    ];
                } else {
                    $contact[$fieldName] = (object)[];
                }
            }

            $contact->organisation_name = $contact->organisation()->value('name');

            if ($pdfs = $contact->pdf()->get()) {
                foreach ($pdfs as $pdf) {
                    $contact[$pdf->type] = [
                        'name' => $pdf->name,
                        'id' => $pdf->pdf_id,
                    ];
                }
            }
        }
        return $contacts;
    }

    /**
     * Create a new contact associated with an organisation.
     */
    public function store(Request $request, Organisation $organisation)
    {
        $contactsFieldsValidation = $this->contactsFieldsValidation;
        $contactsFieldsValidation['tags.*'] = ['integer', Rule::exists('contact_tag', 'contact_tag_id')];
        $contactsFieldsValidation['roles'] = 'required|array';
        $contactsFieldsValidation['roles.*'] = 'string';


        $validator = Validator::make($request->all(), $contactsFieldsValidation);

        $createArray = [];
        $createArrayPDF = [];
        $validator->after(function ($validator) use (&$createArray, &$createArrayPDF) {
            $validated = $validator->validated();

            # Merge the fields de contact rolls. If a field is required, it
            # should remain required after the merge.
            $fieldsValidation = [];
            foreach ($validated['roles'] as $role) {
                foreach ($this->contactRoles[$role] as $fieldName => $required) {
                    if (!array_key_exists($fieldName, $fieldsValidation) || $required) {
                        $fieldsValidation[$fieldName] = $required;
                    }
                }
            }

            foreach (array_keys($this->contactsFieldsValidation) as $fieldName) {
                if (array_key_exists($fieldName, $fieldsValidation)) {
                    if (array_key_exists($fieldName, $validated)) {
                        if ($this->contactFields[$fieldName]['type'] !== 'pdf') {
                            $createArray[$fieldName] = $validated[$fieldName];
                        } else {
                            $createArrayPDF[$fieldName] = $validated[$fieldName];
                        }
                    } else {
                        # Add an error if field is not specified and required.
                        if ($fieldsValidation[$fieldName]) {
                            $validator->errors()->add(
                                $fieldName,
                                'Is required for the given contact rolls.'
                            );
                        }
                    }
                } else {
                    # Add an error if a field was given that was not allowed to
                    # be given.
                    if (array_key_exists($fieldName, $validated)) {
                        $validator->errors()->add(
                            $fieldName,
                            'Is not permitted for the given contact rolls.'
                        );
                    }
                }
            }
        });


        $validated = $validator->validate();

        return DB::transaction(function () use ($validated, $organisation, $createArray, $createArrayPDF) {
            $createArray['organisation_id'] = $organisation->organisation_id;
            $createArray['roles'] = json_encode($validated['roles']);

            $this->logRequest($createArray);

            $contact = Contact::create($createArray);

            if (Gate::allows('assign-contact-tags') && array_key_exists('tags', $validated)) {
                $this->syncTags($organisation, $contact, Auth::user(), $validated['tags']);
            }

            foreach ($createArrayPDF as $pdfType => $pdf) {
                $newPdf = [
                    'name' => $pdf->getClientOriginalName(),
                    'pdf' => base64_encode(file_get_contents($pdf->getRealPath())),
                    'type' => $pdfType,
                ];

                $contact->pdf()->create($newPdf);
            }
            return $contact;
        });
    }

    /**
     * Retrieve a specific contact.
     */
    public function show(Organisation $organisation, Contact $contact)
    {
        $this->logRequest();

        foreach ($this->dropdownFields as $fieldName => $dropdownOption) {
            if ($value = $contact[$fieldName]) {
                $contact[$fieldName] = [
                    'label' => $dropdownOption[$value],
                    'value' => $value
                ];
            } else {
                $contact[$fieldName] = (object)[];
            }
        }
        if ($pdfs = $contact->pdf()->get()) {
            foreach ($pdfs as $pdf) {
                $contact[$pdf->type] = [
                    'name' => $pdf->name,
                    'id' => $pdf->pdf_id,
                ];
            }
        }
        return $contact;
    }

    /**
     * Update a contact.
     */
    public function update(Request $request, Organisation $organisation, Contact $contact)
    {
        $contactsFieldsValidation = $this->contactsFieldsValidation;
        $contactsFieldsValidation['tags.*'] = ['integer', Rule::exists('contact_tag', 'contact_tag_id')];
        $contactsFieldsValidation['roles'] = 'required|array';
        $contactsFieldsValidation['roles.*'] = 'string';

        $validator = Validator::make($request->all(), $contactsFieldsValidation);

        $updateArray = [];
        $updateArrayPDF = [];
        $validator->after(function ($validator) use ($contact, &$updateArray, &$updateArrayPDF) {
            $validated = $validator->validated();

            # Merge the fields de contact rolls. If a field is required, it
            # should remain required after the merge.
            $fieldsValidation = [];
            foreach ($validated['roles'] as $role) {
                foreach ($this->contactRoles[$role] as $fieldName => $required) {
                    if (!array_key_exists($fieldName, $fieldsValidation) || $required) {
                        $fieldsValidation[$fieldName] = $required;
                    }
                }
            }

            foreach (array_keys($this->contactsFieldsValidation) as $fieldName) {
                if (array_key_exists($fieldName, $fieldsValidation)) {
                    if (array_key_exists($fieldName, $validated)) {
                        if ($this->contactFields[$fieldName]['type'] !== 'pdf') {
                            # If the value in the field has been deleted, set the
                            # default value based on the field type for the DB.
                            if ($validated[$fieldName] === null) {
                                if (
                                    in_array(
                                        $this->contactFields[$fieldName]['type'],
                                        ['text',
                                        'text_area',
                                        'phone',
                                        's_mime_cert',
                                        'openpgp_pubkey']
                                    )
                                ) {
                                    $updateArray[$fieldName] = '';
                                } elseif ($this->contactFields[$fieldName]['type'] === 'integer') {
                                    $updateArray[$fieldName] = -1;
                                }
                            } else {
                                $updateArray[$fieldName] = $validated[$fieldName];
                            }
                        } else {
                            $updateArrayPDF[$fieldName] = $validated[$fieldName];
                        }
                    } else {
                        # Add an error if field is not specified and required.
                        # If the PDF is already in the database, it is not required.
                        if (
                            $fieldsValidation[$fieldName] &&
                            $contact->pdf()->where('type', $fieldName)->doesntExist()
                        ) {
                            $validator->errors()->add(
                                $fieldName,
                                'Is required for the given contact rolls.'
                            );
                        }
                    }
                } else {
                    # Add an error if a field was given that was not allowed to
                    # be given.
                    if (array_key_exists($fieldName, $validated)) {
                        $validator->errors()->add(
                            $fieldName,
                            'Is not permitted for the given contact rolls.'
                        );
                    }
                }
            }
        });


        $validated = $validator->validate();

        return DB::transaction(function () use ($validated, $organisation, $contact, $updateArray, $updateArrayPDF) {
            $updateArray['roles'] = json_encode($validated['roles']);

            $this->logRequest($updateArray);

            $contact->update($updateArray);

            if (Gate::allows('assign-contact-tags')) {
                $this->syncTags($organisation, $contact, Auth::user(), $validated['tags'] ?? []);
            }

            foreach ($updateArrayPDF as $pdfType => $pdf) {
                $contact->pdf()->updateOrCreate(
                    ['type' => $pdfType],
                    [
                        'name' => $pdf->getClientOriginalName(),
                        'pdf' => base64_encode(file_get_contents($pdf->getRealPath())),
                        'type' => $pdfType,
                    ],
                );
            }
            return $contact;
        });
    }

    /**
     * Update the contact's tags so that user may assign to the tags in
     * $desiredTags
     *
     * Replace the subset of $contact's tags that $user has permissions for
     * with those in $desiredTags
     *
     * The semantics are somewhat complicated here. The user can usually only
     * work with a subset of all possible tags (e.g. for tenant admins only
     * those associated with the right tenants) but the contact may already
     * have tags that are outside that subset and those tags must not be
     * changed by this method. The new set of tags of the contact is
     * effectively
     *
     *    (OLD - ASSIGNABLE) + (DESIRED & ASSIGNABLE)
     *
     * where
     *
     *    OLD is the set of tags of the contact before this method is called
     *
     *    ASSIGNABLE is the set of tags the user has permissions for and that
     *    belong to a tenant the organisation belongs to
     *
     *    DESIRED is the set of tags in $desiredTags
     *
     *    - is set difference, + is set union and & is set intersection
     */
    public function syncTags(
        Organisation $organisation,
        Contact $contact,
        KeycloakUser $user,
        $desiredTags
    ) {
        // Make sure we're dealing with ints. The client uses
        // multipart/form-data for some requests and there the tags may be
        // given as strings.
        $desiredTags = array_map('intval', $desiredTags);

        // Assignable are all tags that the user may assign and that belong to
        // one of the tenants the organisation belongs to.
        $assignableTags = ContactTag::forUser($user)
            ->whereHas('tenants', function ($query) use ($organisation) {
                $query->whereIn('tenant.tenant_id', $organisation->tenants()->pluck('tenant.tenant_id'));
            })->get();

        // The assigned tags are the tags already assigned to the contact
        $assignedTags = $contact->tags()->get();

        // The tags the contact already has and that the user may not assign
        // must be kept as the user may not modify those.
        $keepTags = $assignedTags->diff($assignableTags);

        // The new set of tags is the union of the kept tags and the
        // intersection of the desired tags and the assignable tags. $keepTags
        // and $assignableTags are disjoint so we can just concat them
        $newTags = $keepTags->concat($assignableTags->only($desiredTags));

        $contact->tags()->sync($newTags);
    }

    /**
     * Remove a contact and its PDF.
     */
    public function destroy(Organisation $organisation, Contact $contact)
    {
        $this->logRequest();
        $contact->delete();
    }
}
