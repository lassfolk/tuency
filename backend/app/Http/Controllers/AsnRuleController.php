<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\Asn;
use App\Models\AsnRule;
use App\Http\Requests\RuleFormRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class AsnruleController extends Controller
{
    /**
     * Retrieve all rules associated with an ASN.
     */
    public function index(Organisation $organisation, Asn $asn)
    {
        return $asn->rules()->with([
            'classificationTaxonomy',
            'classificationType',
            'feedProvider',
            'feedName',
            'contacts',
        ])->get();
    }

    /**
     * Create a new rule associated with an ASN
     */
    public function store(
        RuleFormRequest $request,
        Organisation $organisation,
        Asn $asn
    ) {
        $validated = $request->validated();
        $validated['asn_id'] = $asn->asn_id;

        return DB::transaction(function () use ($validated) {
            $rule = AsnRule::create($validated);

            if (array_key_exists('contacts', $validated)) {
                $rule->contacts()->sync($validated['contacts']);
            }

            return $rule;
        });
    }

    /**
     * Retrieve a specific rule
     */
    public function show(Organisation $organisation, Asn $asn, AsnRule $rule)
    {
        return $rule;
    }

    /**
     * Update a rule.
     */
    public function update(
        RuleFormRequest $request,
        Organisation $organisation,
        Asn $asn,
        AsnRule $rule
    ) {
        $validated = $request->validated();

        return DB::transaction(function () use ($rule, $validated) {
            $rule->update($validated);

            if (array_key_exists('contacts', $validated)) {
                $rule->contacts()->sync($validated['contacts']);
            }

            return $rule;
        });
    }

    /**
     * Remove a rule.
     */
    public function destroy(
        Organisation $organisation,
        Asn $asn,
        AsnRule $rule
    ) {
        $rule->delete();
    }
}
