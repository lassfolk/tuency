<?php

/**
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 * Author: 2020 Bernhard Reiter <bernhard.reiter@intevation.de>
 */

namespace App\Http\Controllers;

use App\Auth\KeycloakUser;
use App\Models\Organisation;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Scito\Laravel\Keycloak\Admin\Facades\KeycloakAdmin;
use Scito\Keycloak\Admin\Exceptions\CannotRetrieveUserException;
use Scito\Keycloak\Admin\Exceptions\CannotCreateUserException;

class KeycloakUsersController extends Controller
{
    private $clientRealm;
    private $clientId;
    private $clientUUID = null;

    public function __construct()
    {
        $clientId = config('tuency.keycloak_admin_tuency_client_id');
        if ($clientId === null) {
            Log::error("tuency.keycloak_admin_tuency_client_id not set");
            abort(500);
        }
        $clientRealm = config('tuency.keycloak_admin_tuency_realm');
        if ($clientRealm === null) {
            Log::error("tuency.keycloak_admin_tuency_realm not set");
            abort(500);
        }

        $this->clientId = $clientId;
        $this->clientRealm = $clientRealm;
    }

    /**
     * Create a pattern for LIKE that matches a given substring.
     *
     * This method replaces '_' with '\_' and '%' with '\%' so that the
     * database does not interpret them as wild-cards and then surrounds
     * the result with '%' wild-cards so that it matches a sub-string.
     */
    private function substringPattern(string $literal)
    {
        $escaped = str_replace('_', '\\_', str_replace('%', '\\%', $literal));
        return "%$escaped%";
    }

    private function getClientUUID()
    {
        if ($this->clientUUID === null) {
            $realm = KeycloakAdmin::realm($this->clientRealm);
            foreach ($realm->clients()->all()->toArray() as $client) {
                if ($client->getClientId() === $this->clientId) {
                    $this->clientUUID = $client->getId();
                }
            }
        }

        if ($this->clientUUID === null) {
            Log::error(
                "Could not determine the UUID for clientId and realm",
                ["clientId" => $this->clientId,
                 "realm" => $this->clientRealm]
            );
            abort(500);
        }
        return $this->clientUUID;
    }

    /**
     * List existing users that the user is allowed to see.
     *
     * With the optional query parameter 'ancestor', which must be the ID of an
     * organisation if specified, the result is restricted to users from that
     * organisation and its direct and indirect sub-organisations.
     *
     * With the optional query parameter 'tenant', which must be the ID of a
     * tenant if specified, the result is restricted to users of
     * organisations that are connected to this tenant directly or indirectly
     * through their parents.
     *
     * If the parameter 'username' is given, only the users matching the given
     * username will be returned.
     *
     * With the optional query parameter 'type' the result is restricted
     * to the colleagues of the user (in the selected node if many are available)
     * type parameter should be one of the values:
     * 'tenantadmin' 'orgaadmin' and the node_id parameter should
     * be defined.
     * The parameter portaladmin can also be specified. Then the node_id
     * parameter is not required.
     *
     * If the optional query parameter 'page' is given the result is
     * paginated. The value of the page parameter should be an integer. Page
     * numbering starts from 1.
     *
     * The parameter 'row' specifies how many entries are to be displayed per
     * page. If "row" is not specified, the default value 10 is used.
     */
    public function index(Request $request)
    {
        $this->logRequest();

        $realm = KeycloakAdmin::realm($this->clientRealm);
        $clientUUID = $this->getClientUUID();
        $result = [];
        $users = $realm->users();

        $validated = $request->validate([
            'ancestor' => 'int|nullable|prohibited_if:tenant,node_id',
            'tenant' => 'int|nullable|prohibited_if:ancestor,node_id',
            'node_id' => 'int|required_if:type,orgaadmin,tenantadmin|prohibited_if:ancestor,tenant',
            'type' => 'string|required_with:node_id|in:orgaadmin,tenantadmin,portaladmin',
            'page' => 'int|nullable',
            'row' => 'int|nullable',
            'username' => 'string|nullable',
            'sort_direction' => 'string|nullable|in:desc,asc',
        ]);

        $ancestor = $validated['ancestor'] ?? null;
        $tenant = $validated['tenant'] ?? null;
        $page = $validated['page'] ?? -1;
        $row = $validated['row'] ?? 10;
        $username = $this->substringPattern($validated['username'] ?? "");
        $sortDirection = $validated['sort_direction'] ?? 'asc';
        $type = $validated['type'] ?? '';
        $nodeId = $validated['node_id'] ?? -1;

        $needsFilter = true;
        $usersIds = [];

        //The portal admin does not need filtering if not wanted.
        if (
            !Auth::user()->isPortalAdmin() ||
            $ancestor ||
            $tenant ||
            $nodeId !== -1
        ) {
            if ($nodeId !== -1) {
                if ($type === 'orgaadmin') {
                    $usersIds = DB::table('organisation_user')->whereIn(
                        'organisation_id',
                        Organisation::forUser(
                            Auth::user()
                        )->where('organisation.organisation_id', $nodeId)
                        ->pluck('organisation.organisation_id')->toArray()
                    )->pluck('organisation_user.keycloak_user_id')->toArray();
                }
                if (
                    $type === 'tenantadmin' &&
                    Tenant::checkAuthorisation(Auth::user(), array(intval($nodeId)))
                ) {
                    $usersIds = DB::table('tenant_user')
                        ->where('tenant_id', $nodeId)
                        ->pluck('keycloak_user_id')->toArray();
                }
            } else {
                $usersIds = DB::table('organisation_user')->whereIn(
                    'organisation_id',
                    Organisation::querySubHierarchy(
                        Auth::user(),
                        $ancestor,
                        $tenant,
                    )->pluck('organisation.organisation_id')
                )->pluck('organisation_user.keycloak_user_id')->toArray();

                // When searching for users of an organisation, those of the tenant
                // should not be returned.
                if (!$ancestor) {
                    // If no tenant is specified, take all tenants that the user is
                    // allowed to see.
                    if ($tenant) {
                        $tenantIds = array($tenant);
                    } else {
                        $tenantIds = DB::table('tenant_user')->where(
                            'keycloak_user_id',
                            Auth::user()->getKeycloakUserId()
                        )->pluck('tenant_user.tenant_id')->toArray();
                    }
                    $tenantUsersIds = DB::table('tenant_user')->whereIn(
                        'tenant_id',
                        $tenantIds
                    )->pluck('tenant_user.keycloak_user_id')->toArray();

                    $usersIds = array_merge($usersIds, $tenantUsersIds);
                }
            }
        } else {
            $needsFilter = false;
        }

        $filteredUsers = [];
        if ($needsFilter) {
            foreach ($users->search(['username' => $username, 'limit' => -1])->get()->toArray() as $user) {
                if (in_array($user->getId(), $usersIds)) {
                    array_push($filteredUsers, $user);
                }
            }
        } else {
            if ($type === 'portaladmin') {
                // Get from the keycloak all users which have the role portaladmin.
                $filteredUsers = $users->search()->getUsersOfRole($clientUUID, "portaladmin");
            } else {
                $filteredUsers = $users->search(['username' => $username, 'limit' => -1])->get()->toArray();
            }
        }

        if ($sortDirection == 'desc') {
            $filteredUsers = array_reverse($filteredUsers);
        }

        $offset = ($page - 1) * $row;

        foreach ($filteredUsers as $user) {
            if ($offset > 0) {
                $offset--;
                continue;
            }

            if ($row <= 0) {
                break;
            }
            $row--;

            $roleMapper = $users->get($user->getId())->roles()->client($clientUUID);
            $roleNames = [];
            foreach ($roleMapper->all()->toArray() as $role) {
                $roleNames[] = $role->getName();
            }

            $keyCloakUser = new KeycloakUser(
                $user->getId(),
                $user->getUsername(),
                $roleNames,
                $this->clientId,
                null,
            );
            $result[] = [
                "id" => $user->getId(),
                "name" => $user->getUsername(),
                "email" => $user->getEmail(),
                "roles" => $roleNames,
                "groups" => $keyCloakUser->resolveGroups(),
            ];
        }

        return [
            'data' => $result,
            'meta' => [
                'total' => sizeof($filteredUsers),
            ],
        ];
    }


    /**
     * Show the Specific User
     */
    public function show($userId)
    {
        $this->logRequest();

        Gate::authorize('show-user-information');

        try {
            $realm = KeycloakAdmin::realm($this->clientRealm);
            $clientUUID = $this->getClientUUID();

            $users = $realm->users();
            $user = $users->get($userId)->toRepresentation();
            $roleMapper = $users->get($user->getId())->roles()->client($clientUUID);
            $roleNames = [];
            foreach ($roleMapper->all()->toArray() as $role) {
                $roleNames[] = $role->getName();
            }

            $keyCloakUser = new KeycloakUser(
                $user->getId(),
                $user->getUsername(),
                $roleNames,
                $this->clientId,
                null,
            );
            return [
                "id" => $userId,
                "name" => $user->getUsername(),
                "last_name" => $user->getLastName(),
                "first_name" => $user->getFirstName(),
                "email" => $user->getEmail(),
                "roles" => $roleNames,
                "groups" => $keyCloakUser->resolveGroups(),
            ];
        } catch (CannotRetrieveUserException $e) {
            abort(410, 'The user is not known (anymore)');
        }
    }

    /**
     * Create a new user
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "email" => "email|required",
            "group" => "in:portaladmin,tenantadmin,orgaadmin|required",
            "node_id" => "int|required_if:group,tenantadmin,orgaadmin",
        ]);


        $validator->after(function ($validator) {
            $validated = $validator->validated();

            if ($validated['group'] === 'portaladmin') {
                Gate::authorize('create-portaladmin');
            } elseif ($validated['group'] === 'tenantadmin') {
                Gate::authorize('create-tenantadmin');

                $user = Auth::user();

                // Determine whether the current user may create a new user
                // for the given tenant. If the current user is a portaladmin
                // we just need to check whether the tenant is known, OTOH if
                // the user is a tenantadmin it must be one of the tenants the
                // current user is responsible for.
                if ($user->isPortalAdmin()) {
                    $query = Tenant::query();
                } elseif ($user->isTenantAdmin()) {
                    $query = DB::table('tenant_user')
                        ->where('keycloak_user_id', $user->getKeycloakUserId());
                }
                $tenantId = $query->where('tenant_id', $validated['node_id'])
                    ->select('tenant_id')->first();

                if ($tenantId === null) {
                    $validator->errors()->add('node_id', 'unknown tenant id');
                }
            } elseif ($validated['group'] === 'orgaadmin') {
                Gate::authorize('create-orgaadmin');

                $user = Auth::user();

                // Determine whether the current user may create a new user
                // for the given organisation. We do this in two steps:
                //
                // 1. determine the set of valid organisation IDs for the
                //    current user. We do this with the Organisation::forUser
                //    method which restricts the set of organisations
                //    depending on the user's groups so that for tenant and
                //    orga admins only those organisations are valid that the
                //    user is associated with
                //
                // 2. check whether the requested organisation ID is in that
                //    set.
                $organisationId = Organisation::forUser($user)
                    ->where(
                        'organisation.organisation_id',
                        $validated['node_id']
                    )->select('organisation.organisation_id')->first();

                if ($organisationId === null) {
                    $validator->errors()->add(
                        'node_id',
                        'unknown organisation id'
                    );
                }
            }
        });

        $validated = $validator->validate();
        $this->logRequest($validated);

        // creating user with keycloak
        // (part of the code is duplicated in routes/console.php
        //  Artisan::command('users:seed {number}'  )

        $requiredActions = ["VERIFY_EMAIL", "UPDATE_PASSWORD",
                            "UPDATE_PROFILE"];

        try {
            $realm = KeycloakAdmin::realm($this->clientRealm);
            $users = $realm->users();
            // Create the user with email address as username
            $creator = $users->create(["username" => $validated["email"],
                                       "email" => $validated["email"],
                                       "enabled" => true]);
            $creator->requiredActions($requiredActions);
            $newUser = $creator->save();

            $client = $realm->client($this->getClientUUID());
            $clientRoles = $client->roles();
            $userClientRoles = $newUser->roles()->client($client->getId());
            $role = $clientRoles->getByName($validated["group"]);
            $userClientRoles->add($role->toRepresentation());

            DB::transaction(function () use ($validated, $newUser) {
                $account = User::create([
                    'keycloak_user_id' => $newUser->getId(),
                ]);

                if ($validated['group'] === 'tenantadmin') {
                    $account->tenants()->sync([$validated['node_id']]);
                } elseif ($validated['group'] === 'orgaadmin') {
                    $account->organisations()->sync([$validated['node_id']]);
                }
            });

            // triggering keycloak to send an "Actions" email

            $requestedClientId = Auth::user()->getClientId();
            $newUser->sendActionsEmail($requiredActions, [
                'client_id' => $requestedClientId,
                'redirect_uri' => $request->headers->get("x-redirect-uri"),
            ]);
        } catch (CannotCreateUserException $e) {
            if ($e->getMessage() === "User exists with same username") {
                abort(409, $e->getMessage());
            } else {
                throw $e;
            }
        }
    }

    public function update(Request $request, User $user)
    {
        $validated = $request->validate([
            'groups' => 'array',
            'groups.*' => 'in:portaladmin,tenantadmin,orgaadmin',
            'tenants' => 'array',
            'tenants.*' => 'int',
            'organisations' => 'array',
            'organisations.*' => 'int',
        ]);
        $this->logRequest($validated);

        $authUser = Auth::user();

        $updatedGroups = $this->syncKeycloakRoles($authUser, $user, $validated['groups'] ?? null);

        DB::transaction(function () use ($authUser, $validated, $user, $updatedGroups) {
            $this->syncTenants(
                $authUser,
                $user,
                $updatedGroups->contains('tenantadmin'),
                $validated['tenants']
            );
            $this->syncOrganisations(
                $authUser,
                $user,
                $updatedGroups->contains('orgaadmin'),
                $validated['organisations']
            );
        });
    }

    /**
     * Return a collection with all roles that the user may assign or remove
     */
    public function managableRoles(KeycloakUser $user)
    {
        // Users can create new users on their level, so they should also be
        // able to add that role to other users. However, it's not so clear
        // that they also should be able to remove that role. For orgaadmins
        // and tenantadmins permissions can be removed by removing the
        // relationship with organisations or tenants. For portaladmins,
        // however, that is not possible, and because it must be possible to
        // remove the portaladmins role, portaladmins can remove all of the
        // roles.
        if ($user->isPortalAdmin()) {
            return collect(['orgaadmin', 'tenantadmin', 'portaladmin']);
        } elseif ($user->isTenantAdmin()) {
            return collect(['orgaadmin', 'tenantadmin']);
        } elseif ($user->isOrgaAdmin()) {
            return collect(['orgaadmin']);
        }

        return collect([]);
    }

    /**
     * Query a Keycloak realm for the tuency roles an account actually has.
     */
    public function clientRoleMapper($realm, User $account)
    {
        $users = $realm->users();
        return $users->get($account->getKey())->roles()->client($this->getClientUUID());
    }

    public function syncKeycloakRoles(KeycloakUser $user, User $account, $desiredRoles)
    {
        // Determine which roles the user is allowed to add or remove.
        $addableRoles = $this->managableRoles($user);

        $clientUUID = $this->getClientUUID();
        $realm = KeycloakAdmin::realm($this->clientRealm);
        $roleMapper = $this->clientRoleMapper($realm, $account);
        $assignedRoles = collect($roleMapper->all())->map(function ($role) {
            return $role->getName();
        });

        if (is_null($desiredRoles)) {
            $desiredRoles = $assignedRoles;
        } else {
            $desiredRoles = collect($desiredRoles)->unique()->values();
        }

        // We need to add roles that are desired and assignable, but not
        // assigned
        $toAdd = $desiredRoles->intersect($addableRoles)->diff($assignedRoles);

        // We need to remove roles that are assigned and delegatable but not
        // in $desiredRoles
        $toRemove = $assignedRoles->intersect($addableRoles)->diff($desiredRoles);
        // Make sure we only remove roles where the user could have assigned
        // all organisations/tenants that are currently associated with the
        // account.
        if (($key = $toRemove->search('orgaadmin')) !== false) {
            $assigned = $account->organisations;
            $assignedAndDelegatable = Organisation::forUser($user)->findMany($assigned->modelKeys());
            if ($assigned->count() !== $assignedAndDelegatable->count()) {
                $toRemove->forget($key);
            }
        }
        if (($key = $toRemove->search('tenantadmin')) !== false) {
            $assigned = $account->tenants;
            $assignedAndDelegatable = Tenant::assignableBy($user)->findMany($assigned->modelKeys());
            if ($assigned->count() !== $assignedAndDelegatable->count()) {
                $toRemove->forget($key);
            }
        }

        $client = $realm->client($clientUUID);
        $clientRoles = $client->roles();
        foreach ($toRemove as $roleName) {
            $role = $clientRoles->getByName($roleName);
            $roleMapper->delete($role->toRepresentation());
        }
        foreach ($toAdd as $roleName) {
            $role = $clientRoles->getByName($roleName);
            $roleMapper->add($role->toRepresentation());
        }

        return $assignedRoles->diff($toRemove)->concat($toAdd);
    }

    /**
     * Determine which tenants associated with an account the user may not remove
     */
    public function unremovableTenants(KeycloakUser $user, User $account)
    {
        // The assigned tenants are the tenants the account is already
        // associated with
        $assignedTenants = $account->tenants;

        // Removable are assigned tenants that the user is allowed to remove.
        $removableTenants = Tenant::assignableBy($user)->findMany($assignedTenants->modelKeys());

        // We need to keep all the tenants the account already has and
        // that the user may not remove.
        return $assignedTenants->diff($removableTenants);
    }

    public function syncTenants(
        KeycloakUser $user,
        User $account,
        $isTenantAdmin,
        $desiredTenants
    ) {
        if ($isTenantAdmin) {
            // We need to keep all the tenants the account already has and
            // that the user may not remove.
            $keepTenants = $this->unremovableTenants($user, $account);

            // Limit desired tenants to actually existing ones that the user
            // can see and is therefore allowed to assign.
            $filteredTenants = Tenant::assignableBy($user)->findMany($desiredTenants);

            // The new set of tenants is the union of the kept tenants and the
            // intersection of the desired tenants and the assignable tenants.
            // $keepTenants and $filteredTenants are disjoint so we can just
            // concat them
            $newTenants = $keepTenants->concat($filteredTenants);
        } else {
            // The account is not a tenant admin, so it must not be associated
            // with any tenents.
            $newTenants = [];
        }

        $account->tenants()->sync($newTenants);
    }

    public function unremovableOrganisations(KeycloakUser $user, User $account)
    {
        // The assigned organisations are the organisations the account is
        // already associated with
        $assignedOrganisations = $account->organisations;

        // Removable are assigned organisations that the user is allowed to
        // remove.
        $removableOrganisations = Organisation::forUser($user)
            ->distinct()
            ->findMany($assignedOrganisations->modelKeys());

        // We need to keep all the organisations the account already has
        // and that the user may not remove.
        return $assignedOrganisations->diff($removableOrganisations);
    }

    public function syncOrganisations(
        KeycloakUser $user,
        User $account,
        $isOrgaAdmin,
        $desiredOrganisations
    ) {
        if ($isOrgaAdmin) {
            // We need to keep all the organisations the account already has
            // and that the user may not remove.
            $keepOrganisations = $this->unremovableOrganisations($user, $account);

            // Limit desired organisations to actually existing ones that the
            // user can see and is therefore allowed to assign.
            $filteredOrganisations = Organisation::forUser($user)->distinct()->findMany($desiredOrganisations);

            // The new set of organisations is the union of the kept
            // organisations and the intersection of the filtered desired
            // organisations and the assignable organisations.
            // $keepOrganisations and $filteredOrganisations are disjoint so
            // we can just concat them
            $newOrganisations = $keepOrganisations->concat($filteredOrganisations);
        } else {
            // The account is not an orgaadmin, so it must not be associated
            // with any organisations.
            $newOrganisations = [];
        }

        $account->organisations()->sync($newOrganisations);
    }

    public function destroy(User $user)
    {
        $this->logRequest();

        $authUser = Auth::user();

        $managableRoles = $this->managableRoles($authUser);

        $realm = KeycloakAdmin::realm($this->clientRealm);
        $roleMapper = $this->clientRoleMapper($realm, $user);
        $assignedRoles = collect($roleMapper->all())->map(function ($role) {
            return $role->getName();
        });

        // Users can only be deleted when the user performing the action can
        // take all of the roles away.
        abort_unless($assignedRoles->diff($managableRoles)->isEmpty(), 403);

        // If the user that is to be deleted is a tenant admin, the user
        // performing the request has to be allowed to take away all of its
        // tenants
        if ($assignedRoles->contains('tenantadmin')) {
            abort_unless($this->unremovableTenants($authUser, $user)->isEmpty(), 403);
        }

        // If the user that is to be deleted is an orga admin, the user
        // performing the request has to be allowed to take away all of its
        // organisations.
        if ($assignedRoles->contains('orgaadmin')) {
            abort_unless($this->unremovableOrganisations($authUser, $user)->isEmpty(), 403);
        }

        DB::transaction(function () use ($user, $realm) {
            $user->delete();

            $users = $realm->users();
            $users->deleteById($user->keycloak_user_id);
        });
    }
}
