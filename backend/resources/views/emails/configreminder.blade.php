Hello {{ $fullname }},
your configuration has not been updated for a while.
Please visit the Tuency application and check if the information is still correct.