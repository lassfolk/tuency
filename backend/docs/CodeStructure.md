## Backend code structure
The directory layout was started with `laravel new` from Laravel v8.10.0.

Our aim is to adhere to https://github.com/php-pds/skeleton
with the following exceptions:
 * For licensing information we follow https://reuse.software/spec/ v3.0
   instead of `LICENSING`.
 * We use `NEWS.md` instead of `CHANGELOG' because it tends more towards
   the notable user facing changes as used by the GNU people
   (see https://gnu.org/prep/standards/html_node/NEWS-File.html).

## Code style
We start out to follow the code-style that Laravel uses
[itself](https://laravel.com/docs/8.x/contributions#coding-style).
And follow
 * [PSR-12: Extended Coding Style](https://www.php-fig.org/psr/psr-12/)
 * [PSR-4: Autoloader](https://www.php-fig.org/psr/psr-4/)
 * [PHPDoc](https://docs.phpdoc.org/3.0/guide/guides/docblocks.html)
   (for documentation in comments)

However we believe that formatting is part of the contents because
code is mainly for other developers to read and understand.
If if makes a section more readable it is okay to deviate a bit
from formatting rules.

One way to check some of the code styling rules is
[PHP Codesniffer](https://pear.php.net/package/PHP_CodeSniffer/)
(Version 3.5.8 or higher).

```sh
phpcs --standard=PSR12
```
