<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFqdnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fqdn', function (Blueprint $table) {
            $table->id('fqdn_id');
            $table->foreignId('organisation_id')
                  ->constrained('organisation', 'organisation_id');
            $table->text('fqdn');
            $table->enum('approval', ['pending', 'approved', 'denied']);

            $table->unique(['organisation_id', 'fqdn']);
            $table->index('fqdn');
            $table->index('approval');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fqdn');
    }
}
