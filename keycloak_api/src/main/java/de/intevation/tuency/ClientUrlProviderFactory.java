package de.intevation.tuency;

import org.keycloak.Config.Scope;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.services.resource.RealmResourceProvider;
import org.keycloak.services.resource.RealmResourceProviderFactory;

/**
 * RealmResourceProviderFactory used to create the ClientUrlProvider.
 * 
 * @author Alexander Woestmann <awoestmann@intevation.de>
 */
public class ClientUrlProviderFactory implements RealmResourceProviderFactory {

    public static final String ID = "clienturlprovider";

    @Override
    public String getId() {
        return ID;
    }

    @Override
    public RealmResourceProvider create(KeycloakSession session) {
        return new ClientUrlProvider(session);
    }

    @Override
    public void init(Scope config) {
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
    }

    @Override
    public void close() {
    }
}
